# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* A quick installation of a NodeJS Socket Server based Single Page App

### Information ###

## Installation Steps ##

```bash	
git clone https://bitbucket.org/davidthorn/exampleapp.git  app

cd app

npm install

node app.js
```
## View the web page ##

 * Open the browser to http://localhost:8000

 * Tada, 


If you want to change the port, you can open the Config.js and change the this.port  to whatever it is that you want